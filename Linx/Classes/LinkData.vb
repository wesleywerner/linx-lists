﻿<Serializable()> _
Public Class LinkData

	Enum TrackingStates
		Unchanged
		Modified
		Added
	End Enum


	''' <summary>
	''' The state of this link item
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property TrackingState() As TrackingStates
		Get
			Return _TrackingState
		End Get
		Set(ByVal value As TrackingStates)
			_TrackingState = value
		End Set
	End Property
	Private _TrackingState As TrackingStates = TrackingStates.Unchanged


	''' <summary>
	''' The display title of the link
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Title() As String
		Get
			Return _Title
		End Get
		Set(ByVal value As String)
			_Title = value
		End Set
	End Property
	Private _Title As String = ""

	''' <summary>
	''' The full path this link points to, directory or application
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Path() As String
		Get
			Return _Path
		End Get
		Set(ByVal value As String)
			_Path = value
		End Set
	End Property
	Private _Path As String = ""

	''' <summary>
	''' The icon offset for this link item
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property IconIndex() As Integer
		Get
			Return _IconIndex
		End Get
		Set(ByVal value As Integer)
			_IconIndex = value
		End Set
	End Property
	Private _IconIndex As Integer = 0

	''' <summary>
	''' Use this alternate icon reference
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property AlternateIconRef() As String
		Get
			Return _AlternateIconRef
		End Get
		Set(ByVal value As String)
			_AlternateIconRef = value
		End Set
	End Property
	Private _AlternateIconRef As String = ""

	''' <summary>
	''' The category for this link
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Category() As String
		Get
			Return _Category
		End Get
		Set(ByVal value As String)
			_Category = value
		End Set
	End Property
	Private _Category As String = ""

	''' <summary>
	''' Arguments to pass the link
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Arguments() As String
		Get
			Return _Arguments
		End Get
		Set(ByVal value As String)
			_Arguments = value
		End Set
	End Property
	Private _Arguments As String = ""

	''' <summary>
	''' The working directory
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property WorkingDir() As String
		Get
			Return _WorkingDir
		End Get
		Set(ByVal value As String)
			_WorkingDir = value
		End Set
	End Property
	Private _WorkingDir As String = ""


	''' <summary>
	''' Constructor
	''' </summary>
	''' <remarks></remarks>
	Public Sub New()

	End Sub

End Class
