﻿_Linx is licensed under the BSD open source license._

== What is this? ==

Linx is an application for Windows 7 that you pin it to the Start Menu or Taskbar. It gives you quick access to shortcuts via the Jump List menu: The Start Menu fly-out, and the Taskbar drag-up mouse gesture. Batteries included.

http://sites.google.com/site/coneware/linx/show-context-menu.png

_Linx leverages the Windows Jump List API, so your links are available even when Linx isn't running!_

That's about it, the built-in quick-start tells you all you need to know. So why not [http://code.google.com/p/linx-lists/source/checkout clone a copy] of the source and build it yourself, if you're into that kinda thing. You know what I'm talking about.

If you prefer a normal build, with a prettily wrapped setup, you can get one in [http://code.google.com/p/linx-lists/downloads/list Downloads], compliments of the pixie-setup-packagers.

== Credit ==

Goes to kyo-tux for the arrow app icon, from his 'MiniIcon' set found at http://kyo-tux.deviantart.com/art/MinIcons-115690703 - Many Thanks!

== Requirements ==

Windows 7 with the .Net framework 3.5 (this is included with Windows 7 by default)

== Questions ==

*Q: Where are Linx settings stored?*

A: Links are saved in %LOCALAPPDATA%\Linx\links.xml, while app-specific settings are saved under a subdirectory in that same path.

*Q: Does Linx support drag-and-drop?*

A: Yes, you can drag a file or application on the Linx taskbar button, hold SHIFT while you do this.

*Q: What are the compiling requirements?*

A: Visual Studio 2008. You also need to link the Windows API Code Pack, since we're building against framework 3.5. Version 4.0 of the framework will have these namespaces built-in, but I haven't compiled against 4 yet. It should work in theory, like most things Microsoft. http://code.msdn.microsoft.com/WindowsAPICodePack