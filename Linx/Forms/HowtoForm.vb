﻿Public Class HowtoForm


	Public Property DefaultUIndex() As Integer
		Get
			Return _DefaultUIndex
		End Get
		Set(ByVal value As Integer)
			_DefaultUIndex = value
		End Set
	End Property
	Private _DefaultUIndex As Integer = 1

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		Me.Close()
	End Sub

	Private Sub Howto_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblHowto3.MouseHover, lblHowto2.MouseHover, lblHowto1.MouseHover

		If sender.Equals(lblHowto1) Then
			toggleUI(1)
		ElseIf sender.Equals(lblHowto2) Then
			toggleUI(2)
		ElseIf sender.Equals(lblHowto3) Then
			toggleUI(3)
		End If

	End Sub

	Private Sub toggleUI(ByVal index As Integer)

		lblHowto1.GradientColorB = SystemColors.GradientActiveCaption
		lblHowto2.GradientColorB = SystemColors.GradientActiveCaption
		lblHowto3.GradientColorB = SystemColors.GradientActiveCaption

		pic1.Visible = False
		pic2.Visible = False
		pic3.Visible = False

		If index = 1 Then
			lblHowto1.GradientColorB = Color.Orange
			pic1.Visible = True
			lblInfo.Text = "Right click the Linx button to pin it"
		ElseIf index = 2 Then
			lblHowto2.GradientColorB = Color.Orange
			pic2.Visible = True
			lblInfo.Text = "Click and drag up to show the links menu"
		ElseIf index = 3 Then
			lblHowto3.GradientColorB = Color.Orange
			pic3.Visible = True
			lblInfo.Text = "Right-click the Start button " & _
			 "-> Properties " & _
			 "-> Customize " & _
			 "-> Change number of items in Jump Lists"
		End If

	End Sub

	Private Sub HowtoForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		' set the form icon
		Me.Icon = My.Resources.app_icon
		toggleUI(DefaultUIndex)

	End Sub

End Class