﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LinkEditForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.tbnAccept = New System.Windows.Forms.ToolStripButton
        Me.tbnCancel = New System.Windows.Forms.ToolStripButton
        Me.txtTitle = New System.Windows.Forms.TextBox
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.ctmBrowse = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmiApplication = New System.Windows.Forms.ToolStripMenuItem
        Me.cmiLocation = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtWorking = New System.Windows.Forms.TextBox
        Me.txtArguments = New System.Windows.Forms.TextBox
        Me.txtAlternateIcon = New System.Windows.Forms.TextBox
        Me.lnkOpenPath = New System.Windows.Forms.LinkLabel
        Me.UltraLabel2 = New Linx.UltraLabel
        Me.UltraLabel6 = New Linx.UltraLabel
        Me.UltraLabel4 = New Linx.UltraLabel
        Me.UltraLabel5 = New Linx.UltraLabel
        Me.UltraLabel3 = New Linx.UltraLabel
        Me.UltraLabel1 = New Linx.UltraLabel
        Me.lnkOpenWorking = New System.Windows.Forms.LinkLabel
        Me.lnkOpenIcon = New System.Windows.Forms.LinkLabel
        Me.ToolStrip1.SuspendLayout()
        Me.ctmBrowse.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtPath
        '
        Me.txtPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPath.Location = New System.Drawing.Point(12, 26)
        Me.txtPath.MaxLength = 500
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(352, 22)
        Me.txtPath.TabIndex = 1
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbnAccept, Me.tbnCancel})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(343, 25)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        Me.ToolStrip1.Visible = False
        '
        'tbnAccept
        '
        Me.tbnAccept.Image = Global.Linx.My.Resources.Resources.dialog_accept2
        Me.tbnAccept.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbnAccept.Name = "tbnAccept"
        Me.tbnAccept.Size = New System.Drawing.Size(64, 22)
        Me.tbnAccept.Text = "&Accept"
        '
        'tbnCancel
        '
        Me.tbnCancel.Image = Global.Linx.My.Resources.Resources.dialog_cancel2
        Me.tbnCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbnCancel.Name = "tbnCancel"
        Me.tbnCancel.Size = New System.Drawing.Size(63, 22)
        Me.tbnCancel.Text = "&Cancel"
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(12, 66)
        Me.txtTitle.MaxLength = 100
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(213, 22)
        Me.txtTitle.TabIndex = 4
        '
        'cboCategory
        '
        Me.cboCategory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(231, 66)
        Me.cboCategory.MaxLength = 50
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(191, 21)
        Me.cboCategory.TabIndex = 6
        '
        'ctmBrowse
        '
        Me.ctmBrowse.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmiApplication, Me.cmiLocation})
        Me.ctmBrowse.Name = "ctmBrowse"
        Me.ctmBrowse.Size = New System.Drawing.Size(121, 48)
        '
        'cmiApplication
        '
        Me.cmiApplication.Name = "cmiApplication"
        Me.cmiApplication.Size = New System.Drawing.Size(120, 22)
        Me.cmiApplication.Text = "&File"
        '
        'cmiLocation
        '
        Me.cmiLocation.Name = "cmiLocation"
        Me.cmiLocation.Size = New System.Drawing.Size(120, 22)
        Me.cmiLocation.Text = "&Location"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = Global.Linx.My.Resources.Resources.dialog_cancel2
        Me.btnCancel.Location = New System.Drawing.Point(347, 227)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.Image = Global.Linx.My.Resources.Resources.dialog_accept2
        Me.btnSave.Location = New System.Drawing.Point(266, 227)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtWorking
        '
        Me.txtWorking.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWorking.Location = New System.Drawing.Point(12, 106)
        Me.txtWorking.MaxLength = 500
        Me.txtWorking.Name = "txtWorking"
        Me.txtWorking.Size = New System.Drawing.Size(352, 22)
        Me.txtWorking.TabIndex = 8
        '
        'txtArguments
        '
        Me.txtArguments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtArguments.Location = New System.Drawing.Point(12, 146)
        Me.txtArguments.MaxLength = 500
        Me.txtArguments.Name = "txtArguments"
        Me.txtArguments.Size = New System.Drawing.Size(410, 22)
        Me.txtArguments.TabIndex = 11
        '
        'txtAlternateIcon
        '
        Me.txtAlternateIcon.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAlternateIcon.Location = New System.Drawing.Point(12, 186)
        Me.txtAlternateIcon.MaxLength = 500
        Me.txtAlternateIcon.Name = "txtAlternateIcon"
        Me.txtAlternateIcon.Size = New System.Drawing.Size(352, 22)
        Me.txtAlternateIcon.TabIndex = 13
        '
        'lnkOpenPath
        '
        Me.lnkOpenPath.ActiveLinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenPath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkOpenPath.BackColor = System.Drawing.SystemColors.Window
        Me.lnkOpenPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lnkOpenPath.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lnkOpenPath.Image = Global.Linx.My.Resources.Resources.find_in_folder
        Me.lnkOpenPath.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkOpenPath.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOpenPath.LinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenPath.Location = New System.Drawing.Point(363, 27)
        Me.lnkOpenPath.Name = "lnkOpenPath"
        Me.lnkOpenPath.Size = New System.Drawing.Size(59, 20)
        Me.lnkOpenPath.TabIndex = 2
        Me.lnkOpenPath.TabStop = True
        Me.lnkOpenPath.Text = "Open"
        Me.lnkOpenPath.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'UltraLabel2
        '
        Me.UltraLabel2.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel2.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel2.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel2.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel2.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel2.Location = New System.Drawing.Point(12, 51)
        Me.UltraLabel2.Name = "UltraLabel2"
        Me.UltraLabel2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel2.Size = New System.Drawing.Size(213, 17)
        Me.UltraLabel2.TabIndex = 3
        Me.UltraLabel2.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel2.Text = "&Title"
        Me.UltraLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UltraLabel6
        '
        Me.UltraLabel6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraLabel6.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel6.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel6.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel6.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel6.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel6.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel6.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel6.Location = New System.Drawing.Point(12, 171)
        Me.UltraLabel6.Name = "UltraLabel6"
        Me.UltraLabel6.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel6.Size = New System.Drawing.Size(410, 17)
        Me.UltraLabel6.TabIndex = 12
        Me.UltraLabel6.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel6.Text = "Alternate &Icon"
        Me.UltraLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UltraLabel4
        '
        Me.UltraLabel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraLabel4.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel4.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel4.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel4.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel4.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel4.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel4.Location = New System.Drawing.Point(12, 91)
        Me.UltraLabel4.Name = "UltraLabel4"
        Me.UltraLabel4.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel4.Size = New System.Drawing.Size(410, 17)
        Me.UltraLabel4.TabIndex = 7
        Me.UltraLabel4.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel4.Text = "&Working Path"
        Me.UltraLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UltraLabel5
        '
        Me.UltraLabel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraLabel5.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel5.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel5.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel5.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel5.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel5.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel5.Location = New System.Drawing.Point(12, 131)
        Me.UltraLabel5.Name = "UltraLabel5"
        Me.UltraLabel5.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel5.Size = New System.Drawing.Size(410, 17)
        Me.UltraLabel5.TabIndex = 10
        Me.UltraLabel5.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel5.Text = "&Arguments"
        Me.UltraLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UltraLabel3
        '
        Me.UltraLabel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraLabel3.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel3.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel3.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel3.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel3.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel3.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel3.Location = New System.Drawing.Point(231, 51)
        Me.UltraLabel3.Name = "UltraLabel3"
        Me.UltraLabel3.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel3.Size = New System.Drawing.Size(191, 17)
        Me.UltraLabel3.TabIndex = 5
        Me.UltraLabel3.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel3.Text = "&Category"
        Me.UltraLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UltraLabel1
        '
        Me.UltraLabel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraLabel1.BackColor = System.Drawing.SystemColors.Window
        Me.UltraLabel1.BorderColor = System.Drawing.SystemColors.Control
        Me.UltraLabel1.BorderStyle = System.Windows.Forms.ButtonBorderStyle.None
        Me.UltraLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.UltraLabel1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.UltraLabel1.GradientColorA = System.Drawing.SystemColors.Control
        Me.UltraLabel1.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
        Me.UltraLabel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.UltraLabel1.Location = New System.Drawing.Point(12, 11)
        Me.UltraLabel1.Name = "UltraLabel1"
        Me.UltraLabel1.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.UltraLabel1.Size = New System.Drawing.Size(410, 17)
        Me.UltraLabel1.TabIndex = 0
        Me.UltraLabel1.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
        Me.UltraLabel1.Text = "Link &Path"
        Me.UltraLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkOpenWorking
        '
        Me.lnkOpenWorking.ActiveLinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenWorking.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkOpenWorking.BackColor = System.Drawing.SystemColors.Window
        Me.lnkOpenWorking.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lnkOpenWorking.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lnkOpenWorking.Image = Global.Linx.My.Resources.Resources.find_in_folder
        Me.lnkOpenWorking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkOpenWorking.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOpenWorking.LinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenWorking.Location = New System.Drawing.Point(363, 106)
        Me.lnkOpenWorking.Name = "lnkOpenWorking"
        Me.lnkOpenWorking.Size = New System.Drawing.Size(59, 20)
        Me.lnkOpenWorking.TabIndex = 9
        Me.lnkOpenWorking.TabStop = True
        Me.lnkOpenWorking.Text = "Open"
        Me.lnkOpenWorking.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkOpenIcon
        '
        Me.lnkOpenIcon.ActiveLinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenIcon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkOpenIcon.BackColor = System.Drawing.SystemColors.Window
        Me.lnkOpenIcon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lnkOpenIcon.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lnkOpenIcon.Image = Global.Linx.My.Resources.Resources.find_in_folder
        Me.lnkOpenIcon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkOpenIcon.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOpenIcon.LinkColor = System.Drawing.SystemColors.WindowText
        Me.lnkOpenIcon.Location = New System.Drawing.Point(363, 186)
        Me.lnkOpenIcon.Name = "lnkOpenIcon"
        Me.lnkOpenIcon.Size = New System.Drawing.Size(59, 20)
        Me.lnkOpenIcon.TabIndex = 14
        Me.lnkOpenIcon.TabStop = True
        Me.lnkOpenIcon.Text = "Open"
        Me.lnkOpenIcon.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LinkEditForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 262)
        Me.Controls.Add(Me.UltraLabel2)
        Me.Controls.Add(Me.UltraLabel6)
        Me.Controls.Add(Me.txtAlternateIcon)
        Me.Controls.Add(Me.UltraLabel4)
        Me.Controls.Add(Me.UltraLabel5)
        Me.Controls.Add(Me.txtArguments)
        Me.Controls.Add(Me.UltraLabel3)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.UltraLabel1)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.cboCategory)
        Me.Controls.Add(Me.txtWorking)
        Me.Controls.Add(Me.txtPath)
        Me.Controls.Add(Me.lnkOpenPath)
        Me.Controls.Add(Me.lnkOpenWorking)
        Me.Controls.Add(Me.lnkOpenIcon)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LinkEditForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Link Editor"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ctmBrowse.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
	Friend WithEvents txtPath As System.Windows.Forms.TextBox
	Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
	Friend WithEvents tbnAccept As System.Windows.Forms.ToolStripButton
	Friend WithEvents tbnCancel As System.Windows.Forms.ToolStripButton
	Friend WithEvents txtTitle As System.Windows.Forms.TextBox
	Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
	Friend WithEvents ctmBrowse As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents cmiApplication As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents cmiLocation As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents UltraLabel1 As Linx.UltraLabel
	Friend WithEvents UltraLabel2 As Linx.UltraLabel
	Friend WithEvents UltraLabel3 As Linx.UltraLabel
	Friend WithEvents txtWorking As System.Windows.Forms.TextBox
	Friend WithEvents UltraLabel4 As Linx.UltraLabel
	Friend WithEvents txtArguments As System.Windows.Forms.TextBox
	Friend WithEvents UltraLabel5 As Linx.UltraLabel
	Friend WithEvents UltraLabel6 As Linx.UltraLabel
	Friend WithEvents txtAlternateIcon As System.Windows.Forms.TextBox
	Friend WithEvents lnkOpenPath As System.Windows.Forms.LinkLabel
	Friend WithEvents lnkOpenWorking As System.Windows.Forms.LinkLabel
	Friend WithEvents lnkOpenIcon As System.Windows.Forms.LinkLabel
End Class
