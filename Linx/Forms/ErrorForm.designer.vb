<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ErrorForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ErrorForm))
		Me.btnClose = New System.Windows.Forms.Button
		Me.txtDetails = New System.Windows.Forms.TextBox
		Me.btnCopy = New System.Windows.Forms.Button
		Me.PictureBox2 = New System.Windows.Forms.PictureBox
		Me.lblZen = New UltraLabel
		CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
		Me.btnClose.Location = New System.Drawing.Point(397, 427)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 23)
		Me.btnClose.TabIndex = 1
		Me.btnClose.Text = "Close"
		Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnClose.UseVisualStyleBackColor = True
		'
		'txtDetails
		'
		Me.txtDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtDetails.BackColor = System.Drawing.Color.White
		Me.txtDetails.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDetails.HideSelection = False
		Me.txtDetails.Location = New System.Drawing.Point(12, 88)
		Me.txtDetails.Multiline = True
		Me.txtDetails.Name = "txtDetails"
		Me.txtDetails.ReadOnly = True
		Me.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtDetails.Size = New System.Drawing.Size(460, 333)
		Me.txtDetails.TabIndex = 2
		'
		'btnCopy
		'
		Me.btnCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCopy.Image = CType(resources.GetObject("btnCopy.Image"), System.Drawing.Image)
		Me.btnCopy.Location = New System.Drawing.Point(12, 427)
		Me.btnCopy.Name = "btnCopy"
		Me.btnCopy.Size = New System.Drawing.Size(127, 23)
		Me.btnCopy.TabIndex = 4
		Me.btnCopy.Text = "&Copy to clipboard"
		Me.btnCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnCopy.UseVisualStyleBackColor = True
		'
		'PictureBox2
		'
		Me.PictureBox2.Image = Global.Linx.My.Resources.Resources.lemmling_Ladybug_small
		Me.PictureBox2.Location = New System.Drawing.Point(12, 12)
		Me.PictureBox2.Name = "PictureBox2"
		Me.PictureBox2.Size = New System.Drawing.Size(60, 70)
		Me.PictureBox2.TabIndex = 9
		Me.PictureBox2.TabStop = False
		'
		'lblZen
		'
		Me.lblZen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.lblZen.BackColor = System.Drawing.SystemColors.Window
		Me.lblZen.BorderColor = System.Drawing.SystemColors.ControlDark
		Me.lblZen.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
		Me.lblZen.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblZen.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lblZen.GradientColorA = System.Drawing.Color.FromArgb(CType(CType(217, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(217, Byte), Integer))
		Me.lblZen.GradientColorB = System.Drawing.SystemColors.Window
		Me.lblZen.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
		Me.lblZen.Location = New System.Drawing.Point(78, 12)
		Me.lblZen.Name = "lblZen"
		Me.lblZen.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
		Me.lblZen.Size = New System.Drawing.Size(394, 70)
		Me.lblZen.TabIndex = 10
		Me.lblZen.TemplateStyle = UltraLabel.TemplateStyles.None
		Me.lblZen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'ErrorForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.CancelButton = Me.btnClose
		Me.ClientSize = New System.Drawing.Size(484, 462)
		Me.Controls.Add(Me.txtDetails)
		Me.Controls.Add(Me.lblZen)
		Me.Controls.Add(Me.PictureBox2)
		Me.Controls.Add(Me.btnCopy)
		Me.Controls.Add(Me.btnClose)
		Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "ErrorForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Error Message"
		CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents btnClose As System.Windows.Forms.Button
	Friend WithEvents txtDetails As System.Windows.Forms.TextBox
	Friend WithEvents btnCopy As System.Windows.Forms.Button
	Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
	Friend WithEvents lblZen As UltraLabel
End Class
