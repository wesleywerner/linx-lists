﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LicenseForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LicenseForm))
		Me.txtLicense = New System.Windows.Forms.TextBox
		Me.btnOkay = New System.Windows.Forms.Button
		Me.splitButtons = New System.Windows.Forms.SplitContainer
		Me.btnAccept = New System.Windows.Forms.Button
		Me.btnDecline = New System.Windows.Forms.Button
		Me.splitButtons.Panel1.SuspendLayout()
		Me.splitButtons.Panel2.SuspendLayout()
		Me.splitButtons.SuspendLayout()
		Me.SuspendLayout()
		'
		'txtLicense
		'
		Me.txtLicense.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtLicense.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicense.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLicense.Location = New System.Drawing.Point(12, 12)
		Me.txtLicense.Multiline = True
		Me.txtLicense.Name = "txtLicense"
		Me.txtLicense.ReadOnly = True
		Me.txtLicense.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtLicense.Size = New System.Drawing.Size(537, 420)
		Me.txtLicense.TabIndex = 1
		'
		'btnOkay
		'
		Me.btnOkay.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnOkay.Image = Global.Linx.My.Resources.Resources.dialog_accept2
		Me.btnOkay.Location = New System.Drawing.Point(49, 9)
		Me.btnOkay.Name = "btnOkay"
		Me.btnOkay.Size = New System.Drawing.Size(75, 23)
		Me.btnOkay.TabIndex = 0
		Me.btnOkay.Text = "&Okay"
		Me.btnOkay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnOkay.UseVisualStyleBackColor = True
		'
		'splitButtons
		'
		Me.splitButtons.Location = New System.Drawing.Point(152, 429)
		Me.splitButtons.Name = "splitButtons"
		'
		'splitButtons.Panel1
		'
		Me.splitButtons.Panel1.Controls.Add(Me.btnDecline)
		Me.splitButtons.Panel1.Controls.Add(Me.btnAccept)
		'
		'splitButtons.Panel2
		'
		Me.splitButtons.Panel2.Controls.Add(Me.btnOkay)
		Me.splitButtons.Size = New System.Drawing.Size(397, 42)
		Me.splitButtons.SplitterDistance = 266
		Me.splitButtons.TabIndex = 3
		'
		'btnAccept
		'
		Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnAccept.Image = Global.Linx.My.Resources.Resources.dialog_accept2
		Me.btnAccept.Location = New System.Drawing.Point(107, 9)
		Me.btnAccept.Name = "btnAccept"
		Me.btnAccept.Size = New System.Drawing.Size(75, 23)
		Me.btnAccept.TabIndex = 0
		Me.btnAccept.Text = "&Accept"
		Me.btnAccept.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnAccept.UseVisualStyleBackColor = True
		'
		'btnDecline
		'
		Me.btnDecline.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnDecline.Image = Global.Linx.My.Resources.Resources.dialog_cancel2
		Me.btnDecline.Location = New System.Drawing.Point(188, 9)
		Me.btnDecline.Name = "btnDecline"
		Me.btnDecline.Size = New System.Drawing.Size(75, 23)
		Me.btnDecline.TabIndex = 1
		Me.btnDecline.Text = "&Decline"
		Me.btnDecline.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnDecline.UseVisualStyleBackColor = True
		'
		'LicenseForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(561, 473)
		Me.Controls.Add(Me.splitButtons)
		Me.Controls.Add(Me.txtLicense)
		Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "LicenseForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "License"
		Me.splitButtons.Panel1.ResumeLayout(False)
		Me.splitButtons.Panel2.ResumeLayout(False)
		Me.splitButtons.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents txtLicense As System.Windows.Forms.TextBox
	Friend WithEvents btnOkay As System.Windows.Forms.Button
	Friend WithEvents splitButtons As System.Windows.Forms.SplitContainer
	Friend WithEvents btnDecline As System.Windows.Forms.Button
	Friend WithEvents btnAccept As System.Windows.Forms.Button
End Class
