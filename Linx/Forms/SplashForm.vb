﻿Public Class SplashForm

	Private opacityOffset As Double = 0.1

	Public Sub New()
		InitializeComponent()
		Me.Opacity = 0.1
		Me.TopMost = True
		tmrFader.Start()
	End Sub

	Private Sub tmrFader_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrFader.Tick
		If Me.Opacity = 1 Then
			tmrFader.Stop()
			tmrAutoClose.Start()
		ElseIf Me.Opacity = 0 Then
			tmrFader.Stop()
			Me.Close()
		Else
			Me.Opacity += opacityOffset
			Application.DoEvents()
		End If
	End Sub

	Private Sub tmrAutoClose_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrAutoClose.Tick
		Me.Opacity = 0.9
		opacityOffset = -0.1
		tmrAutoClose.Stop()
		tmrFader.Start()
	End Sub

	Private Sub SplashForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		' set the form icon
		Me.Icon = My.Resources.app_icon

	End Sub

End Class