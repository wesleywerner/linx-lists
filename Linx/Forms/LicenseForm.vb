﻿Public Class LicenseForm

	Private Sub LicenseForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		' set the form icon
		Me.Icon = My.Resources.app_icon

		' load the license text
		txtLicense.Text = My.Resources.license
		txtLicense.SelectionStart = 0
		txtLicense.SelectionLength = 0

		' show the accept/cancel buttons, or the okay button
		If My.Settings.FirstRun Then
			splitButtons.Panel2Collapsed = True
		Else
			splitButtons.Panel1Collapsed = True
		End If

	End Sub

	Private Sub btnOkay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOkay.Click
		Me.Close()
	End Sub

	Private Sub btnDecline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDecline.Click
		Me.DialogResult = Windows.Forms.DialogResult.Cancel
		Me.Close()
	End Sub

	Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
		Me.DialogResult = Windows.Forms.DialogResult.OK
		Me.Close()
	End Sub
End Class