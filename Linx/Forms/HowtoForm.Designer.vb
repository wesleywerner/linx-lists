﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HowtoForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HowtoForm))
		Me.btnClose = New System.Windows.Forms.Button
		Me.pic1 = New System.Windows.Forms.PictureBox
		Me.pic2 = New System.Windows.Forms.PictureBox
		Me.pic3 = New System.Windows.Forms.PictureBox
		Me.lblInfo = New System.Windows.Forms.Label
		Me.lblHowto3 = New Linx.UltraLabel
		Me.lblHowto2 = New Linx.UltraLabel
		Me.lblHowto1 = New Linx.UltraLabel
		CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.pic2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.pic3, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnClose.Location = New System.Drawing.Point(616, 586)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 23)
		Me.btnClose.TabIndex = 0
		Me.btnClose.Text = "&Close"
		Me.btnClose.UseVisualStyleBackColor = True
		'
		'pic1
		'
		Me.pic1.Image = CType(resources.GetObject("pic1.Image"), System.Drawing.Image)
		Me.pic1.Location = New System.Drawing.Point(272, 9)
		Me.pic1.Name = "pic1"
		Me.pic1.Size = New System.Drawing.Size(328, 187)
		Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me.pic1.TabIndex = 2
		Me.pic1.TabStop = False
		'
		'pic2
		'
		Me.pic2.Image = CType(resources.GetObject("pic2.Image"), System.Drawing.Image)
		Me.pic2.Location = New System.Drawing.Point(272, 9)
		Me.pic2.Name = "pic2"
		Me.pic2.Size = New System.Drawing.Size(338, 544)
		Me.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me.pic2.TabIndex = 3
		Me.pic2.TabStop = False
		Me.pic2.Visible = False
		'
		'pic3
		'
		Me.pic3.Image = CType(resources.GetObject("pic3.Image"), System.Drawing.Image)
		Me.pic3.Location = New System.Drawing.Point(272, 9)
		Me.pic3.Name = "pic3"
		Me.pic3.Size = New System.Drawing.Size(423, 513)
		Me.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me.pic3.TabIndex = 3
		Me.pic3.TabStop = False
		Me.pic3.Visible = False
		'
		'lblInfo
		'
		Me.lblInfo.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblInfo.ForeColor = System.Drawing.Color.ForestGreen
		Me.lblInfo.Location = New System.Drawing.Point(12, 106)
		Me.lblInfo.Name = "lblInfo"
		Me.lblInfo.Size = New System.Drawing.Size(254, 66)
		Me.lblInfo.TabIndex = 4
		'
		'lblHowto3
		'
		Me.lblHowto3.BackColor = System.Drawing.SystemColors.Window
		Me.lblHowto3.BorderColor = System.Drawing.SystemColors.ControlDark
		Me.lblHowto3.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
		Me.lblHowto3.Cursor = System.Windows.Forms.Cursors.Hand
		Me.lblHowto3.Font = New System.Drawing.Font("Segoe UI", 8.25!)
		Me.lblHowto3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lblHowto3.GradientColorA = System.Drawing.SystemColors.Control
		Me.lblHowto3.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
		Me.lblHowto3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
		Me.lblHowto3.Location = New System.Drawing.Point(12, 73)
		Me.lblHowto3.Name = "lblHowto3"
		Me.lblHowto3.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
		Me.lblHowto3.Size = New System.Drawing.Size(254, 21)
		Me.lblHowto3.TabIndex = 1
		Me.lblHowto3.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
		Me.lblHowto3.Text = "3. Increase the max jumlist items to show"
		Me.lblHowto3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'lblHowto2
		'
		Me.lblHowto2.BackColor = System.Drawing.SystemColors.Window
		Me.lblHowto2.BorderColor = System.Drawing.SystemColors.ControlDark
		Me.lblHowto2.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
		Me.lblHowto2.Cursor = System.Windows.Forms.Cursors.Hand
		Me.lblHowto2.Font = New System.Drawing.Font("Segoe UI", 8.25!)
		Me.lblHowto2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lblHowto2.GradientColorA = System.Drawing.SystemColors.Control
		Me.lblHowto2.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
		Me.lblHowto2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
		Me.lblHowto2.Location = New System.Drawing.Point(12, 41)
		Me.lblHowto2.Name = "lblHowto2"
		Me.lblHowto2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
		Me.lblHowto2.Size = New System.Drawing.Size(254, 21)
		Me.lblHowto2.TabIndex = 1
		Me.lblHowto2.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
		Me.lblHowto2.Text = "2. Click+drag up to show your links"
		Me.lblHowto2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'lblHowto1
		'
		Me.lblHowto1.BackColor = System.Drawing.SystemColors.Window
		Me.lblHowto1.BorderColor = System.Drawing.SystemColors.ControlDark
		Me.lblHowto1.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
		Me.lblHowto1.Cursor = System.Windows.Forms.Cursors.Hand
		Me.lblHowto1.Font = New System.Drawing.Font("Segoe UI", 8.25!)
		Me.lblHowto1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lblHowto1.GradientColorA = System.Drawing.SystemColors.Control
		Me.lblHowto1.GradientColorB = System.Drawing.SystemColors.GradientActiveCaption
		Me.lblHowto1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
		Me.lblHowto1.Location = New System.Drawing.Point(12, 9)
		Me.lblHowto1.Name = "lblHowto1"
		Me.lblHowto1.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
		Me.lblHowto1.Size = New System.Drawing.Size(254, 21)
		Me.lblHowto1.TabIndex = 1
		Me.lblHowto1.TemplateStyle = Linx.UltraLabel.TemplateStyles.None
		Me.lblHowto1.Text = "1. Pin Linx to the taskbar"
		Me.lblHowto1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'HowtoForm
		'
		Me.AcceptButton = Me.btnClose
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.CancelButton = Me.btnClose
		Me.ClientSize = New System.Drawing.Size(703, 621)
		Me.Controls.Add(Me.lblInfo)
		Me.Controls.Add(Me.lblHowto3)
		Me.Controls.Add(Me.lblHowto2)
		Me.Controls.Add(Me.lblHowto1)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.pic1)
		Me.Controls.Add(Me.pic2)
		Me.Controls.Add(Me.pic3)
		Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
		Me.Name = "HowtoForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "How to use Linx"
		CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.pic2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.pic3, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents btnClose As System.Windows.Forms.Button
	Friend WithEvents lblHowto1 As Linx.UltraLabel
	Friend WithEvents pic1 As System.Windows.Forms.PictureBox
	Friend WithEvents lblHowto2 As Linx.UltraLabel
	Friend WithEvents lblHowto3 As Linx.UltraLabel
	Friend WithEvents pic2 As System.Windows.Forms.PictureBox
	Friend WithEvents pic3 As System.Windows.Forms.PictureBox
	Friend WithEvents lblInfo As System.Windows.Forms.Label
End Class
