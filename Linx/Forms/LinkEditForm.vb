﻿Public Class LinkEditForm

	''' <summary>
	''' The formatted title of this dialog
	''' </summary>
	''' <remarks></remarks>
	Private Const formTitle As String = "Edit Link - {0}"

	''' <summary>
	''' A list of recently used categories
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Categories() As List(Of String)
		Get
			Return _Categories
		End Get
		Set(ByVal value As List(Of String))
			_Categories = value
		End Set
	End Property
	Private _Categories As List(Of String)

	''' <summary>
	''' The link data object to edit
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property LinkItem() As LinkData
		Get
			Return _LinkItem
		End Get
		Set(ByVal value As LinkData)
			_LinkItem = value
		End Set
	End Property
	Private _LinkItem As LinkData

	''' <summary>
	''' Entry point
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub LinkEditForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		' set the form icon
		Me.Icon = My.Resources.app_icon

		' fix the min size, and allow double the width
		Me.MinimumSize = Me.Size
		Me.MaximumSize = New Size(Me.Width * 2, Me.Height)

		cboCategory.DataSource = Categories
		txtTitle.Text = LinkItem.Title
		txtPath.Text = LinkItem.Path
		cboCategory.Text = LinkItem.Category
		txtWorking.Text = LinkItem.WorkingDir
		txtArguments.Text = LinkItem.Arguments
		txtAlternateIcon.Text = LinkItem.AlternateIconRef
		If txtTitle.Text.Length = 0 Then GuessTitleAndWorkingDir()
	End Sub

	''' <summary>
	''' Browse for a file or folder
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
    Private Sub lnkOpenPath_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOpenPath.LinkClicked
        ctmBrowse.Show(lnkOpenPath, 0, lnkOpenPath.Height)
    End Sub

	Private Sub cmiApplication_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmiApplication.Click
		Dim ofd As New OpenFileDialog
		ofd.Filter = "Best Guess (*.*)|*.*"
		If ofd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
			txtPath.Text = ofd.FileName
			GuessTitleAndWorkingDir
		End If
	End Sub

	Private Sub cmiLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmiLocation.Click
		Dim ofd As New FolderBrowserDialog
		ofd.Description = "Select the location to add"
		If ofd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
			txtPath.Text = ofd.SelectedPath
			GuessTitleAndWorkingDir
		End If
	End Sub

	Private Sub GuessTitleAndWorkingDir()
		If IO.Directory.Exists(txtPath.Text) Then
			txtTitle.Text = IO.Path.GetFileName(txtPath.Text)
			txtWorking.Clear()
		ElseIf IO.File.Exists(txtPath.Text) Then
			txtTitle.Text = IO.Path.GetFileNameWithoutExtension(txtPath.Text)
			txtWorking.Text = IO.Path.GetDirectoryName(txtPath.Text)
		End If
	End Sub

	Private Sub txtTitle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
	Handles txtTitle.TextChanged, txtPath.TextChanged, cboCategory.TextChanged
		btnSave.Enabled = True
		Me.Text = String.Format(formTitle, txtTitle.Text)
	End Sub

	''' <summary>
	''' Accept
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not validate_input Then Exit Sub
        LinkItem.Title = txtTitle.Text
        LinkItem.Path = txtPath.Text
        LinkItem.Category = cboCategory.Text
        LinkItem.WorkingDir = txtWorking.Text
        LinkItem.Arguments = txtArguments.Text
        LinkItem.AlternateIconRef = txtAlternateIcon.Text
        My.Forms.ConfigureForm.UpdateLinkItem(LinkItem)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Function validate_input() As Boolean
        Try
            If txtPath.Text.Length = 0 Then Throw New Exception("Please select a Path")
            If txtTitle.Text.Length = 0 Then Throw New Exception("Please enter a Title")
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "You forgot something", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Function

	''' <summary>
	''' Cancel
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		Me.DialogResult = Windows.Forms.DialogResult.Cancel
		Me.Close()
	End Sub

	''' <summary>
	''' select a working dir
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
    Private Sub lnkOpenWorking_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOpenWorking.LinkClicked
        Dim ofd As New FolderBrowserDialog
        ofd.Description = "Select the workding direcotry"
        If ofd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            txtWorking.Text = ofd.SelectedPath
        End If
    End Sub

	''' <summary>
	''' select an alt icon
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
    Private Sub lnkOpenIcon_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOpenIcon.LinkClicked
        Dim ofd As New OpenFileDialog
        ofd.Filter = "exes (*.exe)|*.exe|Icons (*.ico)|*.ico|Best Guess (*.*)|*.*"
        If ofd.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            txtAlternateIcon.Text = ofd.FileName
        End If
    End Sub

End Class