Public Class ErrorForm

	Private ZEN() As String = { _
	"A file that big?\nIt might be very useful.\nBut now it is gone.", _
	"Yesterday it worked\nToday it is not working\nWindows is like that", _
	"Stay the patient course\nOf little worth is your ire\nThe network is down", _
	"Three things are certain:\nDeath, taxes, and lost data.\nGuess which has occurred.", _
	"You step in the stream,\nbut the water has moved on.\nThis page is not here.", _
	"Out of memory.\nWe wish to hold the whole sky,\nBut we never will.", _
	"Having been erased,\nThe document you're seeking\nMust now be retyped.", _
	"Rather than a beep\nOr a rude error message,\nThese words: 'File not found.'", _
	"Serious error.\nAll shortcuts have disappeared.\nScreen. Mind. Both are blank.", _
	"The Web site you seek\ncannot be located but\nendless others exist", _
	"Chaos reigns within.\nReflect, repent, and reboot.\nOrder shall return.", _
	"ABORTED effort:\nClose all that you have.\nYou ask way too much.", _
	"First snow, then silence.\nThis thousand dollar screen dies\nso beautifully.", _
	"With searching comes loss\nand the presence of absence:\n'My Novel' not found.", _
	"The Tao that is seen\nIs not the true Tao, until\nYou bring fresh toner.", _
	"Windows NT crashed.\nI am the Blue Screen of Death.\nNo one hears your screams.", _
	"A crash reduces\nyour expensive computer\nto a simple stone.", _
	"Error messages\ncannot completely convey.\nWe now know shared loss.", _
	"A simple and hungry Zen monk\norders a pizza:\n'Make me one with everything.'"}


	''' <summary>
	''' Show the error dialog as a modal dialog
	''' </summary>
	''' <param name="ex"></param>
	''' <remarks></remarks>
	Overloads Shared Sub Show(ByVal ex As Exception, Optional ByVal details() As Object = Nothing)
		Dim form As New ErrorForm(ex, details)
		form.ShowDialog()
	End Sub


	''' <summary>
	''' Constructor to the this error form
	''' </summary>
	''' <remarks></remarks>
	Sub New()

		' This call is required by the Windows Form Designer.
		InitializeComponent()

		ErrorForm.Show(New Exception("Lorem ipsum dolor sit amet, consectetur adipiscing elit."))

	End Sub


	''' <summary>
	''' New
	''' </summary>
	''' <param name="ex">The exception</param>
	''' <remarks></remarks>
	Public Sub New(ByVal ex As Exception, ByVal details() As Object)

		' This call is required by the Windows Form Designer.
		InitializeComponent()

		txtDetails.Text = PopMessageInfo(ex) & ControlChars.CrLf & ControlChars.CrLf

		If Not IsNothing(details) Then
			For Each det As Object In details
				If Not IsNothing(det) Then
					txtDetails.Text += det.ToString & ControlChars.CrLf
				End If
			Next
			txtDetails.Text += ControlChars.CrLf
		End If

		txtDetails.Text += ex.ToString
		txtDetails.Select(0, txtDetails.Text.IndexOf(ControlChars.Cr))

		lblZen.Text = GetZen()

	End Sub


	''' <summary>
	''' Close button
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

		Me.DialogResult = Windows.Forms.DialogResult.OK

	End Sub


	''' <summary>
	''' Get a zen message
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Private Function GetZen() As String
		Try
			With New Random
				Return """" & ZEN(.Next(0, ZEN.Length)).Replace("\n", ControlChars.CrLf) & """"
			End With
		Catch ex As Exception
			Return "No insight for now"
		End Try
	End Function


	'// PopMessageInfo
	Private Function PopMessageInfo(ByVal _Exception As Exception) As String

		If TypeOf _Exception Is IO.FileNotFoundException Then
			'-- IO.FileNotFoundException
			Return "The file could not be found. If the file path is a network path, please make sure that the network is accessable."

		ElseIf TypeOf _Exception Is ArgumentException Then
			'-- one of the arguments provided to a method is not valid.
			Return "One of the arguments provided to a method is not valid."

		ElseIf TypeOf _Exception Is ArgumentNullException Then
			'--  a null reference (Nothing in Visual Basic) is passed to a method that does not accept it as a valid argument.
			Return "A null reference was passed to a method that does not accept it as a valid argument."

		ElseIf TypeOf _Exception Is DivideByZeroException Then
			'-- there is an attempt to divide an integral or decimal value by zero. 
			Return "There was an attempt to divide an integral or decimal value by zero."

		ElseIf TypeOf _Exception Is IndexOutOfRangeException Then
			'-- an attempt is made to access an element of an array with an index that is outside the bounds of the array. 
			Return "An attempt was made to access an element of an array with an index that is outside the bounds of the array."

		ElseIf TypeOf _Exception Is InvalidCastException Then
			'-- invalid casting or explicit conversion.
			Return "An invalid casting or explicit conversion was attempted."

		ElseIf TypeOf _Exception Is IO.IsolatedStorage.IsolatedStorageException Then
			'-- an operation in isolated storage fails. 
			Return "An operation in isolated storage failed."

		ElseIf TypeOf _Exception Is NullReferenceException Then
			'-- there is an attempt to dereference a null object reference. 
			Return "There was an attempt to dereference a null object reference."

		ElseIf TypeOf _Exception Is ObjectDisposedException Then
			'-- an operation is performed on a disposed object.
			Return "An operation was performed on a disposed object."

		ElseIf TypeOf _Exception Is OverflowException Then
			'--  an arithmetic, casting, or conversion operation in a checked context results in an overflow.  
			Return "An arithmetic, casting, or conversion operation resulted in an overflow."

			'ElseIf TypeOf _Exception Is SqlException Then
			'	'-- SQL Server returns a warning or error.
			'	Return "The SQL Server returned a warning or error."

		ElseIf TypeOf _Exception Is System.Data.SqlTypes.SqlTypeException Then
			'-- the Value property of a SqlTypes structure is set to null.
			Return "The Value property of a SqlTypes structure is set to null."

		ElseIf TypeOf _Exception Is System.Data.SqlTypes.SqlTruncateException Then
			'-- a value into a SqlType structure would truncate that value.
			Return "A value in a SqlType structure was truncated."

		ElseIf TypeOf _Exception Is Reflection.TargetInvocationException Then
			'-- The exception that is thrown by methods invoked through reflection.
			Return "This exception was thrown by methods invoked through reflection."

		ElseIf TypeOf _Exception Is Reflection.TargetException Then
			'-- an attempt is made to invoke an invalid target.
			Return "An attempt was made to invoke an invalid target."

		ElseIf TypeOf _Exception Is UnauthorizedAccessException Then
			'-- the operating system denies access because of an I/O error or a specific type of security error.
			Return "The operating system denied access because of an I/O error or a specific type of security error."

		Else

			'-- Any other exception
			Return _Exception.Message

		End If

	End Function


	''' <summary>
	''' Copy to clipboard
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
		Dim intro As String = String.Format("Assembly: {0} v{1}{2}{2}", My.Application.Info.AssemblyName, My.Application.Info.Version.ToString, ControlChars.CrLf)
		Try
			txtDetails.SelectAll()
			txtDetails.Focus()

			Dim details As String = ""
			Dim assemblies As String = ""

			For Each asm As Reflection.Assembly In My.Application.Info.LoadedAssemblies
				assemblies += asm.ToString & ControlChars.CrLf
			Next

			details += String.Format("{1}{1}List of loaded assemblies:{1}{0}", assemblies, ControlChars.CrLf)

		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
		Finally
			My.Computer.Clipboard.SetText(intro & txtDetails.Text)
			btnCopy.Text = "Copied!"
		End Try
	End Sub


End Class