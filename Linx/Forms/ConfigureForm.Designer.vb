﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigureForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container
		Me.TabControl1 = New System.Windows.Forms.TabControl
		Me.tabConfig = New System.Windows.Forms.TabPage
		Me.lvwLinks = New System.Windows.Forms.ListView
		Me.clmTitle = New System.Windows.Forms.ColumnHeader
		Me.clmPath = New System.Windows.Forms.ColumnHeader
		Me.ConfigureToolstrip = New System.Windows.Forms.ToolStrip
		Me.tbnAddLink = New System.Windows.Forms.ToolStripButton
		Me.tbnDeleteLink = New System.Windows.Forms.ToolStripButton
		Me.lblStatus = New System.Windows.Forms.ToolStripLabel
		Me.tabAbout = New System.Windows.Forms.TabPage
		Me.PictureBox1 = New System.Windows.Forms.PictureBox
		Me.btnWebsite = New System.Windows.Forms.Button
		Me.btnLicense = New System.Windows.Forms.Button
		Me.lnkDragGesture = New System.Windows.Forms.LinkLabel
		Me.lnkIncreaseMaxItems = New System.Windows.Forms.LinkLabel
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lblVersion = New System.Windows.Forms.Label
		Me.lbltitle = New System.Windows.Forms.Label
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.ctmBrowser = New System.Windows.Forms.ContextMenuStrip(Me.components)
		Me.cmiLocationTool = New System.Windows.Forms.ToolStripMenuItem
		Me.cmiApplicationOrFileTool = New System.Windows.Forms.ToolStripMenuItem
		Me.btnRefreshJumplist = New System.Windows.Forms.Button
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
		Me.TabControl1.SuspendLayout()
		Me.tabConfig.SuspendLayout()
		Me.ConfigureToolstrip.SuspendLayout()
		Me.tabAbout.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.ctmBrowser.SuspendLayout()
		Me.SuspendLayout()
		'
		'TabControl1
		'
		Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TabControl1.Controls.Add(Me.tabConfig)
		Me.TabControl1.Controls.Add(Me.tabAbout)
		Me.TabControl1.Location = New System.Drawing.Point(12, 12)
		Me.TabControl1.Name = "TabControl1"
		Me.TabControl1.SelectedIndex = 0
		Me.TabControl1.Size = New System.Drawing.Size(560, 479)
		Me.TabControl1.TabIndex = 0
		'
		'tabConfig
		'
		Me.tabConfig.Controls.Add(Me.lvwLinks)
		Me.tabConfig.Controls.Add(Me.ConfigureToolstrip)
		Me.tabConfig.Location = New System.Drawing.Point(4, 22)
		Me.tabConfig.Name = "tabConfig"
		Me.tabConfig.Padding = New System.Windows.Forms.Padding(3)
		Me.tabConfig.Size = New System.Drawing.Size(552, 453)
		Me.tabConfig.TabIndex = 2
		Me.tabConfig.Text = "Configure Linx"
		Me.tabConfig.UseVisualStyleBackColor = True
		'
		'lvwLinks
		'
		Me.lvwLinks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.clmTitle, Me.clmPath})
		Me.lvwLinks.Dock = System.Windows.Forms.DockStyle.Fill
		Me.lvwLinks.FullRowSelect = True
		Me.lvwLinks.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
		Me.lvwLinks.HideSelection = False
		Me.lvwLinks.Location = New System.Drawing.Point(3, 28)
		Me.lvwLinks.MultiSelect = False
		Me.lvwLinks.Name = "lvwLinks"
		Me.lvwLinks.Size = New System.Drawing.Size(546, 422)
		Me.lvwLinks.TabIndex = 0
		Me.lvwLinks.UseCompatibleStateImageBehavior = False
		Me.lvwLinks.View = System.Windows.Forms.View.Details
		'
		'clmTitle
		'
		Me.clmTitle.Text = "Title"
		'
		'clmPath
		'
		Me.clmPath.Text = "Path"
		'
		'ConfigureToolstrip
		'
		Me.ConfigureToolstrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbnAddLink, Me.tbnDeleteLink, Me.lblStatus})
		Me.ConfigureToolstrip.Location = New System.Drawing.Point(3, 3)
		Me.ConfigureToolstrip.Name = "ConfigureToolstrip"
		Me.ConfigureToolstrip.Size = New System.Drawing.Size(546, 25)
		Me.ConfigureToolstrip.TabIndex = 0
		Me.ConfigureToolstrip.Text = "ToolStrip2"
		'
		'tbnAddLink
		'
		Me.tbnAddLink.Image = Global.Linx.My.Resources.Resources.add
		Me.tbnAddLink.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.tbnAddLink.Name = "tbnAddLink"
		Me.tbnAddLink.Size = New System.Drawing.Size(74, 22)
		Me.tbnAddLink.Text = "&Add Link"
		'
		'tbnDeleteLink
		'
		Me.tbnDeleteLink.Enabled = False
		Me.tbnDeleteLink.Image = Global.Linx.My.Resources.Resources.delete2
		Me.tbnDeleteLink.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.tbnDeleteLink.Name = "tbnDeleteLink"
		Me.tbnDeleteLink.Size = New System.Drawing.Size(85, 22)
		Me.tbnDeleteLink.Text = "&Delete Link"
		'
		'lblStatus
		'
		Me.lblStatus.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.lblStatus.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
		Me.lblStatus.ForeColor = System.Drawing.SystemColors.GrayText
		Me.lblStatus.Name = "lblStatus"
		Me.lblStatus.Size = New System.Drawing.Size(59, 22)
		Me.lblStatus.Text = "Loading..."
		'
		'tabAbout
		'
		Me.tabAbout.Controls.Add(Me.PictureBox1)
		Me.tabAbout.Controls.Add(Me.btnWebsite)
		Me.tabAbout.Controls.Add(Me.btnLicense)
		Me.tabAbout.Controls.Add(Me.lnkDragGesture)
		Me.tabAbout.Controls.Add(Me.lnkIncreaseMaxItems)
		Me.tabAbout.Controls.Add(Me.Label4)
		Me.tabAbout.Controls.Add(Me.Label3)
		Me.tabAbout.Controls.Add(Me.Label2)
		Me.tabAbout.Controls.Add(Me.Label1)
		Me.tabAbout.Controls.Add(Me.lblVersion)
		Me.tabAbout.Controls.Add(Me.lbltitle)
		Me.tabAbout.Location = New System.Drawing.Point(4, 22)
		Me.tabAbout.Name = "tabAbout"
		Me.tabAbout.Padding = New System.Windows.Forms.Padding(3)
		Me.tabAbout.Size = New System.Drawing.Size(552, 453)
		Me.tabAbout.TabIndex = 0
		Me.tabAbout.Text = "About Linx"
		Me.tabAbout.UseVisualStyleBackColor = True
		'
		'PictureBox1
		'
		Me.PictureBox1.Image = Global.Linx.My.Resources.Resources.logo_cat_x48
		Me.PictureBox1.Location = New System.Drawing.Point(29, 27)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
		Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me.PictureBox1.TabIndex = 9
		Me.PictureBox1.TabStop = False
		'
		'btnWebsite
		'
		Me.btnWebsite.Image = Global.Linx.My.Resources.Resources.web_browser
		Me.btnWebsite.Location = New System.Drawing.Point(143, 346)
		Me.btnWebsite.Name = "btnWebsite"
		Me.btnWebsite.Size = New System.Drawing.Size(118, 23)
		Me.btnWebsite.TabIndex = 3
		Me.btnWebsite.Text = "Visit &Website"
		Me.btnWebsite.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnWebsite.UseVisualStyleBackColor = True
		'
		'btnLicense
		'
		Me.btnLicense.Image = Global.Linx.My.Resources.Resources.View_doc_outline
		Me.btnLicense.Location = New System.Drawing.Point(19, 346)
		Me.btnLicense.Name = "btnLicense"
		Me.btnLicense.Size = New System.Drawing.Size(118, 23)
		Me.btnLicense.TabIndex = 2
		Me.btnLicense.Text = "View &License"
		Me.btnLicense.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.btnLicense.UseVisualStyleBackColor = True
		'
		'lnkDragGesture
		'
		Me.lnkDragGesture.ActiveLinkColor = System.Drawing.Color.Blue
		Me.lnkDragGesture.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lnkDragGesture.Location = New System.Drawing.Point(26, 160)
		Me.lnkDragGesture.Name = "lnkDragGesture"
		Me.lnkDragGesture.Size = New System.Drawing.Size(207, 20)
		Me.lnkDragGesture.TabIndex = 0
		Me.lnkDragGesture.TabStop = True
		Me.lnkDragGesture.Text = "Show me how..."
		'
		'lnkIncreaseMaxItems
		'
		Me.lnkIncreaseMaxItems.ActiveLinkColor = System.Drawing.Color.Blue
		Me.lnkIncreaseMaxItems.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lnkIncreaseMaxItems.Location = New System.Drawing.Point(26, 217)
		Me.lnkIncreaseMaxItems.Name = "lnkIncreaseMaxItems"
		Me.lnkIncreaseMaxItems.Size = New System.Drawing.Size(133, 19)
		Me.lnkIncreaseMaxItems.TabIndex = 1
		Me.lnkIncreaseMaxItems.TabStop = True
		Me.lnkIncreaseMaxItems.Text = "Show me how..."
		'
		'Label4
		'
		Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(26, 192)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(487, 25)
		Me.Label4.TabIndex = 5
		Me.Label4.Text = "By default the jump list only shows 10 items, but you can easily increase this va" & _
			"lue."
		'
		'Label3
		'
		Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(26, 254)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(454, 21)
		Me.Label3.TabIndex = 4
		Me.Label3.Text = "Now switch over to the ""Configure"" tab, and start adding your links!"
		'
		'Label2
		'
		Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(26, 118)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(494, 42)
		Me.Label2.TabIndex = 3
		Me.Label2.Text = "Hello, if this is your first time, pin me to the taskbar. Access your links via t" & _
			"he button right-click menu, or better yet, the click-drag gesture!"
		'
		'Label1
		'
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(163, 43)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(357, 32)
		Me.Label1.TabIndex = 2
		Me.Label1.Text = """Linx provides quick access to directories and applications, leveraging Windows 7" & _
			" jump lists and the taskbar pin functionality."""
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'lblVersion
		'
		Me.lblVersion.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblVersion.Location = New System.Drawing.Point(83, 59)
		Me.lblVersion.Name = "lblVersion"
		Me.lblVersion.Size = New System.Drawing.Size(57, 17)
		Me.lblVersion.TabIndex = 1
		Me.lblVersion.Text = "version"
		Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'lbltitle
		'
		Me.lbltitle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbltitle.Location = New System.Drawing.Point(83, 27)
		Me.lbltitle.Name = "lbltitle"
		Me.lbltitle.Size = New System.Drawing.Size(57, 32)
		Me.lbltitle.TabIndex = 0
		Me.lbltitle.Text = "Linx"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.Image = Global.Linx.My.Resources.Resources.dialog_cancel2
		Me.btnCancel.Location = New System.Drawing.Point(493, 497)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 23)
		Me.btnCancel.TabIndex = 3
		Me.btnCancel.Text = "&Close"
		Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip1.SetToolTip(Me.btnCancel, "Close without saving new changes")
		Me.btnCancel.UseVisualStyleBackColor = True
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnSave.Enabled = False
		Me.btnSave.Image = Global.Linx.My.Resources.Resources.dialog_accept2
		Me.btnSave.Location = New System.Drawing.Point(380, 497)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(107, 23)
		Me.btnSave.TabIndex = 2
		Me.btnSave.Text = "&Save && Close"
		Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip1.SetToolTip(Me.btnSave, "Save your changes, and refresh the jump list")
		Me.btnSave.UseVisualStyleBackColor = True
		'
		'ctmBrowser
		'
		Me.ctmBrowser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmiLocationTool, Me.cmiApplicationOrFileTool})
		Me.ctmBrowser.Name = "ctmBrowser"
		Me.ctmBrowser.Size = New System.Drawing.Size(169, 48)
		'
		'cmiLocationTool
		'
		Me.cmiLocationTool.Name = "cmiLocationTool"
		Me.cmiLocationTool.Size = New System.Drawing.Size(168, 22)
		Me.cmiLocationTool.Text = "&Location"
		'
		'cmiApplicationOrFileTool
		'
		Me.cmiApplicationOrFileTool.Name = "cmiApplicationOrFileTool"
		Me.cmiApplicationOrFileTool.Size = New System.Drawing.Size(168, 22)
		Me.cmiApplicationOrFileTool.Text = "&Application or file"
		'
		'btnRefreshJumplist
		'
		Me.btnRefreshJumplist.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnRefreshJumplist.Image = Global.Linx.My.Resources.Resources.refresh
		Me.btnRefreshJumplist.Location = New System.Drawing.Point(16, 497)
		Me.btnRefreshJumplist.Name = "btnRefreshJumplist"
		Me.btnRefreshJumplist.Size = New System.Drawing.Size(80, 23)
		Me.btnRefreshJumplist.TabIndex = 1
		Me.btnRefreshJumplist.Text = "&Refresh"
		Me.btnRefreshJumplist.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
		Me.ToolTip1.SetToolTip(Me.btnRefreshJumplist, "Refresh the taskbar jump list to match your changes")
		Me.btnRefreshJumplist.UseVisualStyleBackColor = True
		'
		'ConfigureForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(584, 532)
		Me.Controls.Add(Me.btnRefreshJumplist)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.TabControl1)
		Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Name = "ConfigureForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Linx"
		Me.TabControl1.ResumeLayout(False)
		Me.tabConfig.ResumeLayout(False)
		Me.tabConfig.PerformLayout()
		Me.ConfigureToolstrip.ResumeLayout(False)
		Me.ConfigureToolstrip.PerformLayout()
		Me.tabAbout.ResumeLayout(False)
		Me.tabAbout.PerformLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ctmBrowser.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
	Friend WithEvents tabAbout As System.Windows.Forms.TabPage
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents ctmBrowser As System.Windows.Forms.ContextMenuStrip
	Friend WithEvents cmiLocationTool As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents cmiApplicationOrFileTool As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents tabConfig As System.Windows.Forms.TabPage
	Friend WithEvents lvwLinks As System.Windows.Forms.ListView
	Friend WithEvents ConfigureToolstrip As System.Windows.Forms.ToolStrip
	Friend WithEvents tbnAddLink As System.Windows.Forms.ToolStripButton
	Friend WithEvents tbnDeleteLink As System.Windows.Forms.ToolStripButton
	Friend WithEvents clmTitle As System.Windows.Forms.ColumnHeader
	Friend WithEvents clmPath As System.Windows.Forms.ColumnHeader
	Friend WithEvents btnRefreshJumplist As System.Windows.Forms.Button
	Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
	Friend WithEvents lblVersion As System.Windows.Forms.Label
	Friend WithEvents lbltitle As System.Windows.Forms.Label
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents lnkDragGesture As System.Windows.Forms.LinkLabel
	Friend WithEvents lnkIncreaseMaxItems As System.Windows.Forms.LinkLabel
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents btnWebsite As System.Windows.Forms.Button
	Friend WithEvents btnLicense As System.Windows.Forms.Button
	Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
	Friend WithEvents lblStatus As System.Windows.Forms.ToolStripLabel
End Class
