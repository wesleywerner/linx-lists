﻿Namespace My

	' The following events are available for MyApplication:
	' 
	' Startup: Raised when the application starts, before the startup form is created.
	' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
	' UnhandledException: Raised if the application encounters an unhandled exception.
	' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
	' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
	Partial Friend Class MyApplication

		Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
			CheckArguments(e.CommandLine.ToList)
		End Sub

		Private Sub MyApplication_StartupNextInstance(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs) Handles Me.StartupNextInstance
			CheckArguments(e.CommandLine.ToList)
		End Sub

		Private Sub CheckArguments(ByVal args As List(Of String))
			For Each arg In args
				Dim item As New LinkData
				item.Path = args(0)
				Dim form As New LinkEditForm()
				My.Forms.ConfigureForm.Show()
				form.Owner = My.Forms.ConfigureForm
				form.LinkItem = item
				form.Show()
			Next
		End Sub

	End Class

End Namespace

