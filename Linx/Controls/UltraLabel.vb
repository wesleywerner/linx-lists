Public Class UltraLabel
    Inherits System.Windows.Forms.Label


#Region " enumerations "

    ''' <summary>
    ''' Defines a list of predefined template styles
    ''' </summary>
    ''' <remarks></remarks>
    Enum TemplateStyles

        ''' <summary>
        ''' no style
        ''' </summary>
        ''' <remarks></remarks>
        None

        ''' <summary>
        ''' info or hint
        ''' </summary>
        ''' <remarks></remarks>
        Hint

        ''' <summary>
        ''' heading
        ''' </summary>
        ''' <remarks></remarks>
        Heading

        ''' <summary>
        ''' warning
        ''' </summary>
        ''' <remarks></remarks>
        Warning

    End Enum


#End Region


#Region " properties "


    Private _TemplateStyle As TemplateStyles
    Public Property TemplateStyle() As TemplateStyles
        Get
            'Return _TemplateStyle
            Return TemplateStyles.None
        End Get
        Set(ByVal value As TemplateStyles)
            '_TemplateStyle = value

            Select Case value
                'Case TemplateStyles.None
                '    BorderStyle = ButtonBorderStyle.None
                '    BorderColor = SystemColors.Control
                '    BackColor = SystemColors.Control
                '    ForeColor = SystemColors.ControlText
                '    Font = New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)
                '    'TextAlign = ContentAlignment.MiddleLeft
                '    'AutoSize = True   

                Case TemplateStyles.Hint
                    BorderStyle = ButtonBorderStyle.Dotted
                    BorderColor = SystemColors.ControlDarkDark
                    BackColor = SystemColors.Info
                    ForeColor = SystemColors.InfoText
                    Font = New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Italic)
                    GradientColorA = SystemColors.Info
                    GradientColorB = SystemColors.Info
                    GradientMode = Drawing2D.LinearGradientMode.Vertical
                    TextAlign = ContentAlignment.MiddleLeft

                Case TemplateStyles.Heading
                    BorderStyle = ButtonBorderStyle.Solid
                    BorderColor = SystemColors.ControlDark
                    BackColor = SystemColors.Window
                    ForeColor = SystemColors.WindowText
                    Font = New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)
                    GradientColorA = SystemColors.Control
                    GradientColorB = SystemColors.GradientActiveCaption
                    GradientMode = Drawing2D.LinearGradientMode.Vertical
                    TextAlign = ContentAlignment.MiddleCenter

                Case TemplateStyles.Warning
                    BorderStyle = ButtonBorderStyle.Solid
                    BorderColor = Color.Crimson
                    BackColor = SystemColors.Info
                    ForeColor = SystemColors.ControlText
                    Font = New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
                    GradientColorA = SystemColors.Window
                    GradientColorB = Color.MistyRose
                    GradientMode = Drawing2D.LinearGradientMode.Vertical
                    TextAlign = ContentAlignment.MiddleCenter

            End Select

        End Set
    End Property


    Private _BorderStyle As Windows.Forms.ButtonBorderStyle
    Public Shadows Property BorderStyle() As Windows.Forms.ButtonBorderStyle
        Get
            Return _BorderStyle
        End Get
        Set(ByVal value As Windows.Forms.ButtonBorderStyle)
            _BorderStyle = value
            Me.Invalidate()
        End Set
    End Property


    Private _BorderColor As System.Drawing.Color
    Public Property BorderColor() As System.Drawing.Color
        Get
            Return _BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            _BorderColor = value
            Me.Invalidate()
        End Set
    End Property


    Private _GradientMode As Drawing2D.LinearGradientMode = Drawing2D.LinearGradientMode.Vertical
    Public Property GradientMode() As Drawing2D.LinearGradientMode
        Get
            Return _GradientMode
        End Get
        Set(ByVal value As Drawing2D.LinearGradientMode)
            _GradientMode = value
            Me.Invalidate()
        End Set
    End Property


    Private _GradientColorA As Color = SystemColors.Control
    Public Property GradientColorA() As Color
        Get
            Return _GradientColorA
        End Get
        Set(ByVal value As Color)
            _GradientColorA = value
            Me.Invalidate()
        End Set
    End Property


    Private _GradientColorB As Color = SystemColors.Control
    Public Property GradientColorB() As Color
        Get
            Return _GradientColorB
        End Get
        Set(ByVal value As Color)
            _GradientColorB = value
            Me.Invalidate()
        End Set
    End Property


#End Region


    ''' <summary>
    ''' paint the background of the control
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
	Protected Overrides Sub OnPaintBackground(ByVal e As System.Windows.Forms.PaintEventArgs)

		' create the linear gradient brush
		Dim b As New Drawing2D.LinearGradientBrush(Me.ClientRectangle, GradientColorA, GradientColorB, GradientMode)

		' only render the clip retangle, ie the area being validated
		e.Graphics.FillRectangle(b, e.ClipRectangle)

		' draw the border
		ControlPaint.DrawBorder(e.Graphics, Me.ClientRectangle, BorderColor, BorderStyle)

		' clean up
		b.Dispose()

	End Sub

	Public Sub New()

		' This call is required by the Windows Form Designer.
		InitializeComponent()

		Me.Padding = New Padding(10, 0, 0, 0)

	End Sub



End Class
