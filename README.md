_Linx is licensed under the BSD open source license._

# Latest News
April 2015 - Migrate code to bitbucket, Google code is shutting down.
June 2010 - Revision 1 is out, we have a new app icon!

# What is Linx?
Linx is an application for Windows 7 that you pin it to the Start Menu and Taskbar. It gives you quick access to shortcuts via the Jump List menu: The Start Menu fly-out, and the Taskbar drag-up mouse gesture. Batteries included.

![linx-site-taskbar.png](https://bitbucket.org/repo/g9XXd8/images/3107253018-linx-site-taskbar.png)
pinned to the task bar

![linx-site-startmenu.png](https://bitbucket.org/repo/g9XXd8/images/2989401772-linx-site-startmenu.png)
pinned to the start menu

# More Details
Linx leverages the Windows Jump List API, so your links are available even when Linx isn't running!

That's about it, the built-in quick-start tells you all you need to know. So why not clone a copy of the source and build it yourself, if you're into that kinda thing. You know what I'm talking about.

If you prefer a normal build, with a prettily wrapped setup, you can get one in Downloads, compliments of the pixie-setup-packagers.

# Requirements
Windows 7 with the .Net framework 3.5 (this is included with Windows 7 by default)

# Questions
Q: Where are Linx settings stored?

A: Links are saved in %LOCALAPPDATA%\Linx\links.xml, while app-specific settings are saved under a subdirectory in that same path.

Q: What are the compiling requirements?

A: Visual Studio 2008. You also need to link the Windows API Code Pack, since we're building against framework 3.5. Version 4.0 of the framework will have these namespaces built-in, but I haven't compiled against 4 yet. It should work in theory, like most things Microsoft. http://code.msdn.microsoft.com/WindowsAPICodePack